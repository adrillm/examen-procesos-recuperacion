import org.ietf.jgss.Oid;

import java.io.*;
import java.util.*;

public class Ejercicio3 {
    public static void main(String[] args) {
        String comando = "java -jar UnaNuevaEsperanza.jar";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        lista.addAll(Arrays.asList(args));



        try {
            ProcessBuilder pb = new ProcessBuilder(lista);
            Process p = pb.start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
            Scanner sc = new Scanner(System.in);
            String line;
            while (!(line = sc.nextLine()).equals("stop")) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(br.readLine());
            }

            System.out.println(line);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
