import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2Rebels {
    public static void main(String[] args) {
        String comando = "java -jar UnaNuevaEsperanza.jar -i";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        BufferedReader br = null;
        BufferedWriter bw = null;
        ProcessBuilder pb = new ProcessBuilder(lista);
        System.out.print("Introduce el nombre del personaje ");

        try {
            Process p = pb.start();
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            bw = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

            Scanner sc = new Scanner(System.in);
            String linea;

            while (!(linea = sc.nextLine()).equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println("La frase del personaje es " + br.readLine());
            }
            System.out.println(linea);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}

