import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        String comando = "java -jar UnaNuevaEsperanza.jar -i ";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(lista);

        try {

            Process p = pb.start();
            pb.inheritIO();
            Scanner sc = new Scanner(System.in);
            String line;
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while (!(line = sc.nextLine()).equals("stop")) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(br.readLine());
            }

        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}
