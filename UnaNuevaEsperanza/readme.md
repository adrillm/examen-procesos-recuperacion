# Una nueva esperanza

La aplicación generada a partir de este código UnaNuevaEsperanza.jar tiene la siguiente funcionalidad:

Se le introduce un nombre de un personaje de la Saga de la Guerra de las Galaxias (la única y verdadera) devuelve 
aleatoriamente una de las frases más conocidas del personaje. Éste comando puede funcionar de varias maneras:

## En modo comando

Se ejcuta en la línea de órdenes y muestra por la salida estándar:

Por ejemplo: 

```bash

java -jar UnaNuevaEsperanza.jar Leia
¿Puede alguien quitarme de delante a este felpudo con patas?

```

Si lo volvemos a ejecutar, puede que devuelva:

```bash

java -jar UnaNuevaEsperanza.jar Leia
Ayúdame Obi-Wan Kenobi... Eres mi única esperanza...

```

En caso de que no ter al personaje o haberle pasado parámetros incorrectos daria el siguiente resultado por la salida
de error:

```bash

java -jar UnaNuevaEsperanza.jar Pepe
El personaje Pepe no se encuentra en el diccionario 

#  Devolveria un valor distinto de 0
```

## En modo interactivo

El programa, además de en modo comando que es su ejecución por defecto, puede ejecutarse en modo interactivo mediante el 
parámetro `-i` con este parámetro, lo que hacemos es entrar en un modo en el cual vamos introduciendo personajes o el 
comando `?` y nos devuelve el resultado. En caso de tener una frase para el personaje la devuelve por la salida estandar
y en caso de no tenerlo lo devuelve por la salida de error. con `stop` finalizamos la ejecución:

Ejemplo:

```bash
java -jar UnaNuevaEsperanza.jar -i
Leia
¿Puede alguien quitarme de delante a este felpudo con patas?
Pepe
Error: Personaje no encontrado
?
Leia Obi-Wan
Obi-Wan
Si me matas, me convertiré en mas poderoso de lo que jamás puedas imaginar
stop
```

## Frases personalizadas.

Tanto en el modo comando como en el modo interactivo, podemos utilizar un archivo de frases personalizadas. 

Para el modo comando lo ejecutariamos como `java -jar UnaNuevaEsperanza.jar -d frases.json Leia` y para el modo y para el modo
interactivo como `java -jar UnaNuevaEsperanza.jar -i -d frases.json`

## Comando -h

Com el parámetro `-h` mostramos la ayuda
