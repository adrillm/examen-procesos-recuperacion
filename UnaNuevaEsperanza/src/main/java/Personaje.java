public class Personaje {

    String nombre;

    String[] frases;

    public Personaje(String nombre) {
        this.nombre = nombre;
    }

    public String[] getFrases() {
        return frases;
    }

    public void setFrases(String[] frases) {
        this.frases = frases;
    }


}
