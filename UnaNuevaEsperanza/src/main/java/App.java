

import com.google.gson.Gson;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


public class App {

    //Definiciones
    private static final String DESCONOCIDO = "desconocido";
    public static final String STOP = "stop";

    //Exit OK
    private static final int EXIT_OK = 0;

    //Error ids
    private static final int EXIT_ERROR_NAME_NOT_FOUND = 1;
    private static final int EXIT_ERROR_UNKNOWN = 10;
    private static final int EXIT_ERROR_WRONG_ARGS = 11;
    private static final int EXIT_ERROR_DICT_FILE_NOT_EXIST = 12;
    private static final int EXIT_ERROR_DICT_FILE_WRONG_JSON = 13;

    //Commands
    private static final String COMMAND_LINE_INTERACTIVE = "-i";
    private static final String COMMAND_LINE_HELP = "-h";
    private static final String COMMAND_LINE_DICT = "-d";
    private static final String COMMAND_LINE_SHOW_CHARACTERS = "-l";


    private static final String JAR_RUN_HELP = "java -jar SimpsonsDic.jar";

    public static void main(String[] args) {


        String fichero_frases_txt = null;
        //Por defecto, se ejecutará en modo commando
        ExecutionMode executionMode = ExecutionMode.COMMAND;
        String personaje_txt = null;
        Map<String,Personaje> frases = new HashMap<String, Personaje>();


        if (args.length == 1){

            switch (args[0]){
                case COMMAND_LINE_INTERACTIVE:
                    executionMode = ExecutionMode.INTERACTIVE;
                    break;
                case COMMAND_LINE_HELP:
                    executionMode = ExecutionMode.HELP;
                    break;
                case COMMAND_LINE_SHOW_CHARACTERS:
                    executionMode = ExecutionMode.SHOW_CHARACTERS;
                    break;
                default:
                    personaje_txt = args[0];

            }

        } else if (args.length == 2){

            if (args[0].equalsIgnoreCase(COMMAND_LINE_DICT)) {
                fichero_frases_txt = args[1];
            } else {
                System.exit(EXIT_ERROR_WRONG_ARGS);
            }

        } else if ( args.length == 3 ){

            if (args[0].equalsIgnoreCase(COMMAND_LINE_INTERACTIVE)
            && args[1].equalsIgnoreCase(COMMAND_LINE_DICT))
            {
                executionMode = ExecutionMode.INTERACTIVE;
                fichero_frases_txt = args[2];
            } else if (args[0].equalsIgnoreCase(COMMAND_LINE_SHOW_CHARACTERS)
                    && args[1].equalsIgnoreCase(COMMAND_LINE_DICT))
            {
                executionMode = ExecutionMode.SHOW_CHARACTERS;
                fichero_frases_txt = args[2];
            } else {
                printHelp(System.err);
                System.exit(EXIT_ERROR_WRONG_ARGS);
            }

        } else {

            System.err.println("Parámetros Incorrectos");
            printHelp(System.err);
            System.exit(EXIT_ERROR_WRONG_ARGS);

        }

        //Lógica del diccionario

        if (fichero_frases_txt != null){
            File fichero_frases_f = new File(fichero_frases_txt);
            CargarFrasesDesdeFichero(frases,fichero_frases_f);
        } else {
            CargarFrasesPorDefecto(frases);
        }

        switch (executionMode){

            case HELP:
                printHelp(System.out);
                break;
            case COMMAND:
                runCommandMode(personaje_txt,frases);
                break;
            case INTERACTIVE:
                startInteractiveMode(personaje_txt,frases);
                break;
            case SHOW_CHARACTERS:
                showCharactersAndQuotes(personaje_txt,frases);
                break;
            default:
                System.exit(EXIT_ERROR_UNKNOWN);

        }

        System.exit(0);

    }

    private static void CargarFrasesDesdeFichero(Map<String, Personaje> frases, File fichero_frases_f) {

        Gson gson = new Gson();

        try {
            Personaje[] personajes = gson.fromJson(
                    new FileReader(fichero_frases_f),
                    Personaje[].class
            );
            for (Personaje p : personajes ) {
                frases.put(p.nombre,p);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void showCharactersAndQuotes(String personaje_txt, Map<String, Personaje> frases) {

        System.out.println("Mostrando los personajes y sus frases cargadas");
        System.out.println();

        frases.forEach((nombre,personaje) ->{
            System.out.println(nombre);
            Arrays.stream(personaje.frases).map( (x) -> "\t" + x).forEach(System.out::println);
        });
    }

    private static void startInteractiveMode(String personaje_txt, Map<String, Personaje> frases) {

        Scanner sc = new Scanner(System.in);

        String linea = "";

        while (linea != STOP) {
            linea = sc.nextLine();

            if (linea.equalsIgnoreCase("?")) {

                frases.forEach((x, y) -> {
                    System.out.print(y.nombre + " ");
                });

                System.out.println();

            } else if (frases.containsKey(linea)) {

                Personaje p = frases.get(linea);
                String frase = p.frases[ThreadLocalRandom.current().nextInt(0, p.getFrases().length)];
                System.out.println(frase);
            } else if (linea.equalsIgnoreCase(STOP)){
                return;
            }else {

                System.err.println("Error: Personaje no encontrado");

            }


        }

    }

    private static void runCommandMode(String personaje_txt, Map<String, Personaje> frases) {

        if (!frases.containsKey(personaje_txt)){
           System.err.println("El personaje " + personaje_txt + " no se encuentra en el diccionario ");
           System.exit(EXIT_ERROR_NAME_NOT_FOUND);
        }

        Personaje p = frases.get(personaje_txt);

        String frase = p.frases[ThreadLocalRandom.current().nextInt(0,p.getFrases().length)];

        System.out.println(frase);
    }

    private static void CargarFrasesPorDefecto(Map<String, Personaje> frases) {

        Personaje obiwan = new Personaje("Obi-Wan");

        obiwan.frases = new String[]{
                "La fuerza estará ya contigo... siempre",
                "Tus ojos pueden engañarte, no confíes en ellos.",
                "¿Quién es más loco: el loco o el loco que sigue al loco?",
                "Si me matas, me convertiré en mas poderoso de lo que jamás puedas imaginar"
        };

        frases.put(obiwan.nombre, obiwan);

        Personaje leia = new Personaje("Leia");

        leia.setFrases(new String[]{
                "¿Puede alguien quitarme de delante a este felpudo con patas?",
                "Ayúdame Obi-Wan Kenobi... Eres mi única esperanza...",
        });

        frases.put(leia.nombre, leia);

    }

    private static void printHelp(PrintStream out) {

        out.println("Más información en https://gitlab.com/cipfpbatoi/dam/2122/psp/laguerradelasgalaxiasfrases/-/blob/main/UnaNuevaEsperanza/readme.md");

    }

}
