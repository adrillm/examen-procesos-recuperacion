public enum ExecutionMode {

    ERROR,
    HELP,
    COMMAND,
    INTERACTIVE,
    SHOW_CHARACTERS
}
