import java.io.*;
import java.util.*;

public class Ejercicio1 {
    public static void main(String[] args) {
        String comando = "java -jar UnaNuevaEsperanza.jar Leia";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        BufferedReader br;
        ProcessBuilder pb = new ProcessBuilder(lista);

        try {
            Process p = pb.start();
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String linea = br.readLine();

            System.out.println(linea);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
